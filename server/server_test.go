package server

import (
	pb "blockchain/blockchain_proto"
	"github.com/stretchr/testify/assert"
	"golang.org/x/net/context"
	"testing"
)

func TestServer_GetBlockInvalidNetworkCode(t *testing.T) {
	s := Server{}
	blockReq := &pb.BlockRequest{
		BlockHash:   "test-hash",
		NetworkCode: "abc",
	}
	_, err := s.GetBlock(context.Background(), blockReq)
	assert.EqualError(t, err, "invalid network code")
}

func TestServer_GetBlockInvalidHash(t *testing.T) {
	s := Server{}
	blockReq := &pb.BlockRequest{
		BlockHash:   "test-hash",
		NetworkCode: "BTC",
	}
	_, err := s.GetBlock(context.Background(), blockReq)
	assert.NotNil(t, err)
}

func TestServer_GetBlock(t *testing.T) {
	s := Server{}
	blockReq := &pb.BlockRequest{
		BlockHash:   "000000000000034a7dedef4a161fa058a2d67a173a90155f3a2fe6fc132e0ebf",
		NetworkCode: "BTC",
	}
	block, err := s.GetBlock(context.Background(), blockReq)
	assert.Nil(t, err)
	assert.Equal(t, 10, len(block.Transactions))
	assert.Equal(t, int64(200000), block.BlockNumber)
	assert.Equal(t, "2012-09-22 12:45:59", block.Timestamp)
}

func TestServer_GetTransactionInvalidNetworkCode(t *testing.T) {
	s := Server{}
	txRequest := &pb.TransactionRequest{
		TxHash:      "test-hash",
		NetworkCode: "abc",
	}
	_, err := s.GetTransaction(context.Background(), txRequest)
	assert.EqualError(t, err, "invalid network code")
}

func TestServer_GetTransactionInvalidHash(t *testing.T) {
	s := Server{}
	txRequest := &pb.TransactionRequest{
		TxHash:      "test-hash",
		NetworkCode: "BTC",
	}
	_, err := s.GetTransaction(context.Background(), txRequest)
	assert.NotNil(t, err)
}

func TestServer_GetTransaction(t *testing.T) {
	s := Server{}
	txRequest := &pb.TransactionRequest{
		TxHash:      "ee475443f1fbfff84ffba43ba092a70d291df233bd1428f3d09f7bd1a6054a1f",
		NetworkCode: "BTC",
	}
	tx, err := s.GetTransaction(context.Background(), txRequest)
	assert.Nil(t, err)
	assert.Equal(t, "102.12000000", tx.SentValue)
	assert.Equal(t, "ee475443f1fbfff84ffba43ba092a70d291df233bd1428f3d09f7bd1a6054a1f", tx.TxID)

}
