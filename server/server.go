package server

import (
	pb "blockchain/blockchain_proto"
	"blockchain/util"
	"golang.org/x/net/context"
	"log"
)

type Server struct {
	pb.UnimplementedBlockchainServer
}

func (s *Server) GetBlock(ctx context.Context, blockRequest *pb.BlockRequest) (*pb.Block, error) {
	url := "https://sochain.com/api/v2/get_block/" + blockRequest.NetworkCode + "/" + blockRequest.BlockHash
	resp, err := util.GetModelInfo(blockRequest.NetworkCode, url)
	if err != nil {
		log.Printf("GetBlock: error while processing the request: %v", err)
		return nil, err
	}

	transactions, err := getFirstTenTransactions(ctx, s, resp)
	if err != nil {
		log.Printf("GetBlock: error while fetching first ten transactions: %v", err)
		return nil, err
	}
	block := &pb.Block{
		BlockNumber:   int64(resp.Data.BlockNo),
		Timestamp:     util.FormatTime(resp.Data.Time),
		PrevBlockHash: resp.Data.PreviousBlockhash,
		NextBlockHash: resp.Data.NextBlockhash,
		Size:          int32(resp.Data.Size),
		Transactions:  transactions,
	}

	return block, nil
}

func (s *Server) GetTransaction(ctx context.Context, transactionRequest *pb.TransactionRequest) (*pb.Transaction, error) {
	url := "https://sochain.com/api/v2/tx/" + transactionRequest.NetworkCode + "/" + transactionRequest.TxHash
	resp, err := util.GetModelInfo(transactionRequest.NetworkCode, url)
	if err != nil {
		log.Printf("GetTransaction: error while processing the request with Network %v and"+
			" Hash %v : %v", transactionRequest.NetworkCode, transactionRequest.TxHash, err)
		return nil, err
	}
	transaction := &pb.Transaction{
		TxID:      resp.Data.Txid,
		Timestamp: util.FormatTime(resp.Data.Time),
		TxFee:     resp.Data.Fee,
		SentValue: resp.Data.SentValue,
	}
	return transaction, nil
}

// TODO: clarify invalid tx hash (with https://sochain.com/api/v2/get_tx/ url)
// works well with https://sochain.com/api/v2/tx/ url
func getFirstTenTransactions(ctx context.Context, s *Server, resp util.Response) ([]*pb.Transaction, error) {
	var transactions []*pb.Transaction
	txCount := 0
	for i := 0; i < len(resp.Data.Txs); i++ {
		txRequest := &pb.TransactionRequest{
			TxHash:      resp.Data.Txs[i],
			NetworkCode: resp.Data.Network,
		}
		txData, err := s.GetTransaction(ctx, txRequest)
		if err != nil {
			log.Println("getFirstTenTransactions: " + err.Error())
			continue
		}
		transaction := pb.Transaction{
			TxID:      txData.TxID,
			Timestamp: txData.Timestamp,
			TxFee:     txData.TxFee,
			SentValue: txData.SentValue,
		}
		transactions = append(transactions, &transaction)
		txCount += 1
		if txCount == 10 {
			break
		}
	}
	return transactions, nil
}

//func (s *server) getFirstTenTransactions(ctx context.Context, resp response) ([]*pb.Transaction, error) {
//	var transactions []*pb.Transaction
//	for i := 0; i < 10; i++ {
//		txRequest := &pb.TransactionRequest{
//			TxHash:      resp.Data.Txs[i],
//			NetworkCode: resp.Data.Network,
//		}
//		txData, err := s.GetTransaction(ctx, txRequest)
//		if err != nil {
//			return nil, err
//		}
//		transactions[i].TxID = resp.Data.Txs[i]
//		transactions[i].TxFee = txData.TxFee
//		transactions[i].SentValue = txData.SentValue
//		transactions[i].Timestamp = txData.Timestamp
//	}
//	return transactions, nil
//}
