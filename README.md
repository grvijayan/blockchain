A gRPC implementation to retrieve block and transaction information. This implementation makes use of an [external API](https://sochain.com/api/)
#### Assumptions

1. The user who's making the request is authorized and authenticated to make the request

#### To run

(The implementation uses gRPC to help run it as a standalone microservice (to help with scalability), if necessary)

1. Run the `main.go` file under the parent directory to start the server (`go run main.go`)
2. In a separate terminal, run client/client.go (`go run client/client.go <request_type> <network_code> <hash>`)

   Example 1: `go run client/client.go block BTC 000000000000034a7dedef4a161fa058a2d67a173a90155f3a2fe6fc132e0ebf`

   Example 2: `go run client/client.go tx BTC 2674c8a46b75e5a5e3287a34d7999a1e5e6f052be36f63f3cb483ec148c9b86c`
3. Play around with different hashes and network codes

#### Further additions/improvements

1. Authorization/Authentication check
2. Customize unmarshalling of various HTTP responses from external API (block, tx, errors, etc.)
3. Customize error messages according to various failure scenarios (e.g. incorrect network and hash combination)
4. To deal with latency and delays in external API calls, we could make use of go routines and channels to make multiple HTTP requests,
   especially when obtaining the first 10 transactions
5. To deploy the microservice, we could containerize the application using Docker and deploy it in a Kubernetes cluster
6. Implement centralized logging in case the application is deployed on multiple hosts