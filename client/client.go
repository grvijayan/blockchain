package main

import (
	pb "blockchain/blockchain_proto"
	"encoding/json"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"log"
	"os"
	"time"
)

const (
	address = "localhost:50051"
)

func main() {
	// Set up a connection to the server.
	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer func(conn *grpc.ClientConn) {
		err := conn.Close()
		if err != nil {
			log.Fatalf("error closing connection: %v", err)
		}
	}(conn)
	c := pb.NewBlockchainClient(conn)

	// Contact the server and print out its response.
	if len(os.Args) < 4 {
		log.Println("Usage: <request_type> <network_code> <hash>")
		log.Println("Example 1: block BTC 000000000000034a7dedef4a161fa058a2d67a173a90155f3a2fe6fc132e0ebf")
		log.Println("Example 2: tx BTC 2674c8a46b75e5a5e3287a34d7999a1e5e6f052be36f63f3cb483ec148c9b86c")
		log.Fatalln("Please provide a valid input.")
	}
	requestType := os.Args[1]
	netcode := os.Args[2]
	hash := os.Args[3]
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	var resp interface{}
	switch requestType {
	case "block":
		resp, err = c.GetBlock(ctx, &pb.BlockRequest{BlockHash: hash, NetworkCode: netcode})
		if err != nil {
			log.Fatal(err)
		}
	case "tx":
		resp, err = c.GetTransaction(ctx, &pb.TransactionRequest{TxHash: hash, NetworkCode: netcode})
		if err != nil {
			log.Fatal(err)
		}
	default:
		log.Fatalln("Please provide a valid request type: block or tx")
	}

	out, err := json.MarshalIndent(resp, "", "  ")
	if err != nil {
		return
	}
	log.Printf("%s\n", string(out))
}
