package util

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"time"
)

func decodeResponse(httpResp *http.Response) (Response, error) {
	// non-200 error from the API
	if httpResp.StatusCode != 200 {
		return Response{}, errors.New(httpResp.Status)
	}
	resp := Response{}
	err := json.NewDecoder(httpResp.Body).Decode(&resp)
	if err != nil {
		return resp, errors.New("unable to decode the response: " + err.Error())
	}
	if resp.Status == "fail" {
		return resp, errors.New(resp.Data.Blockid + resp.Data.Txid)
	}
	return resp, nil
}

func GetModelInfo(networkCode, url string) (Response, error) {
	if (networkCode != "BTC") && (networkCode != "LTC") && networkCode != "DOGE" {
		return Response{}, errors.New("invalid network code")
	}
	log.Printf("fetching data from %v", url)
	httpResp, err := http.Get(url)
	if err != nil {
		return Response{}, err
	}
	defer httpResp.Body.Close()

	resp, err := decodeResponse(httpResp)
	if err != nil {
		return resp, err
	}
	return resp, nil
}

func FormatTime(timestamp int) string {
	t := time.Unix(int64(timestamp), 0)
	layout := "2006-01-02 15:04:05"
	return t.Format(layout)
}

type Response struct {
	Status string `json:"status"`
	Data   struct {
		Blockid           string   `json:"blockid"`
		MiningDifficulty  string   `json:"mining_difficulty"`
		IsOrphan          bool     `json:"is_orphan"`
		Txs               []string `json:"txs"`
		Merkleroot        string   `json:"merkleroot"`
		PreviousBlockhash string   `json:"previous_blockhash"`
		NextBlockhash     string   `json:"next_blockhash"`
		Network           string   `json:"network"`
		Txid              string   `json:"txid"`
		Blockhash         string   `json:"blockhash"`
		BlockNo           int      `json:"block_no"`
		Confirmations     int      `json:"confirmations"`
		Time              int      `json:"time"`
		Size              int      `json:"size"`
		Vsize             int      `json:"vsize"`
		Version           int      `json:"version"`
		Locktime          int      `json:"locktime"`
		SentValue         string   `json:"sent_value"`
		Fee               string   `json:"fee"`
		Inputs            []struct {
			InputNo      int    `json:"input_no"`
			Address      string `json:"address"`
			Value        string `json:"value"`
			ReceivedFrom struct {
				Txid     string `json:"txid"`
				OutputNo int    `json:"output_no"`
			} `json:"received_from"`
			ScriptAsm string      `json:"script_asm"`
			ScriptHex string      `json:"script_hex"`
			Witness   interface{} `json:"witness"`
		} `json:"inputs"`
		Outputs []struct {
			OutputNo int         `json:"output_no"`
			Address  string      `json:"address"`
			Value    string      `json:"value"`
			Type     string      `json:"type"`
			ReqSigs  interface{} `json:"req_sigs"`
			Spent    struct {
				Txid    string `json:"txid"`
				InputNo int    `json:"input_no"`
			} `json:"spent"`
			ScriptAsm string `json:"script_asm"`
			ScriptHex string `json:"script_hex"`
		} `json:"outputs"`
		TxHex string `json:"tx_hex"`
	} `json:"data"`
	Code    int    `json:"code"`
	Message string `json:"message"`
}

//type response struct {
//	Status string `json:"status"`
//	Data   struct {
//		Network           string   `json:"network"`
//		Blockhash         string   `json:"blockhash"`
//		BlockNo           int      `json:"block_no"`
//		MiningDifficulty  string   `json:"mining_difficulty"`
//		Time              int      `json:"time"`
//		Confirmations     int      `json:"confirmations"`
//		IsOrphan          bool     `json:"is_orphan"`
//		Txs               []string `json:"txs"`
//		Merkleroot        string   `json:"merkleroot"`
//		PreviousBlockhash string   `json:"previous_blockhash"`
//		NextBlockhash     string   `json:"next_blockhash"`
//		Size              int      `json:"size"`
//	} `json:"data"`
//}

//type blockErrorResponse struct {
//	Status string `json:"status"`
//	Data   struct {
//		Network string `json:"network"`
//		Blockid string `json:"blockid"`
//	} `json:"data"`
//}

//type txErrorResponse struct {
//	Status string `json:"status"`
//	Data   struct {
//		Network string `json:"network"`
//		Txid    string `json:"txid"`
//	} `json:"data"`
//}
